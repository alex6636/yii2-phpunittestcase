<?php
namespace alexs\yii2phpunittestcase\tests;

class ApplicationTest extends \alexs\yii2phpunittestcase\TestCase
{
    public function testApp() {
        $this->assertInstanceOf(\yii\web\Application::class, \Yii::$app);
    }

    public function testContainer() {
        $this->assertInstanceOf(\yii\di\Container::class, \Yii::$container);
    }
}