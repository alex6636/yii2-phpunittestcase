<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-May-17
 */

namespace alexs\yii2phpunittestcase;
use yii\web\Application;

abstract class DatabaseTestCase extends TestCase
{
    protected $dsn = 'sqlite:tests/sqlite.db';
    
    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=> dirname(__DIR__) . '/vendor',
            'components'=>[
                'db'=>[
                    'class'=>'yii\db\Connection',
                    'dsn'=>$this->dsn,
                ],
            ],
        ]);
    }
}
