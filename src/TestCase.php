<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-May-21
 */

namespace alexs\yii2phpunittestcase;
use Yii;
use yii\di\Container;
use yii\web\Application;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function setUp():void {
        parent::setUp();
        $this->mockApplication();
    }

    protected function tearDown():void {
        $this->destroyApplication();
        parent::tearDown();
    }

    protected function mockApplication() {
        new Application([
            'id'=>'test_app',
            'basePath'=>__DIR__,
            'vendorPath'=>dirname(__DIR__) . '/vendor',
        ]);
    }

    protected function destroyApplication() {
        Yii::$app = null;
        Yii::$container = new Container();
    }
}
