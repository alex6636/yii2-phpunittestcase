<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-May-21
 */

namespace alexs\yii2phpunittestcase;
use Yii;

abstract class DatabaseTableTestCase extends DatabaseTestCase
{
    abstract protected function getTableName();

    abstract protected function getTableColumns();

    protected function setUp():void {
        parent::setUp();
        Yii::$app->db->createCommand()->createTable($this->getTableName(), $this->getTableColumns())->execute();
    }

    protected function tearDown():void {
        Yii::$app->db->createCommand()->dropTable($this->getTableName())->execute();
        parent::tearDown();
    }
}
